/**
 * Values reused in styling of application
 */
export default {
  maxContainerWidth: "960px",
  inputHeight: "2.25rem",
  inputFontSize: "0.85rem",
  cardHeight: "36rem",
  defaultBackgroundColor: "#edecec",
  defaultImageBackgroundColor: "#d8d8d8",
  menubarBackgroundColor: "#454545",
  inputBackgroundColor: "#313131",
  inputTextColor: "#d8d8d8",
  buttonBackgroundColor: "#c64849",
  buttonHoverOpacity: "0.85",
  buttonTransition: "all ease-in 0.2s"
};
