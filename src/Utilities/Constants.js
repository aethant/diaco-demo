/**
 * Singleton to provide quick access to constant values used in application
 *
 * @class Constants
 */
class Constants {
  constructor() {
    if (!Constants.instance) {
      this._data = [];

      this._data.NEWSAPI_KEY = process.env.REACT_APP_NEWSAPI_KEY; // accessing process.env is an expensive operation
      this._data.NEWSAPI_TOP_HEADLINES = "https://newsapi.org/v2/top-headlines";
      this._data.NEWSAPI_EVERYTHING = "https://newsapi.org/v2/everything";
    }

    return this;
  }

  /**
   * Retrieve NEWSAPI API access key, passed via command line
   *
   * @readonly
   *
   * @memberOf Constants
   */
  get newsAPIKey() {
    return this._data.NEWSAPI_KEY;
  }

  /**
   * Retrieve the "Top Headlines" endpoint URL with key affixed
   *
   * @readonly
   *
   * @memberOf Constants
   */
  get newsAPITopHeadlinesURL() {
    return `${this._data.NEWSAPI_TOP_HEADLINES}?apiKey=${
      this._data.NEWSAPI_KEY
    }`;
  }

  /**
   * Retrieve the "Everything" endpoint URL with key affixed
   *
   * @readonly
   *
   * @memberOf Constants
   */
  get newsAPIEverythingURL() {
    return `${this._data.NEWSAPI_EVERYTHING}?apiKey=${this._data.NEWSAPI_KEY}`;
  }
}

export default new Constants();
