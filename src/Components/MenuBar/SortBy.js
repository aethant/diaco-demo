import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { withStyles } from "@material-ui/core/styles";

/**
 * Describes select options to sort by. Minimize repeated code in our component render body.
 */
const sortOptions = [
  { value: "none", label: "Sort Articles" },
  { value: "relevancy", label: "Sort by relevence" },
  { value: "publishedAt", label: "Sort by date" },
  { value: "popularity", label: "Sort by popularity" }
];

/**
 * Renders a select input to sort articles by predetermined filters, as
 * described in API documentation
 *
 * @prop {Function} handler         handle input changes to component
 * @prop {String}   value           value applied to our component
 */
const SortBy = ({ handler, value }) => (
  <SelectWrapper>
    <StyledSelect onChange={handler} value={value}>
      {sortOptions.map(({ value, label }) => (
        <MenuItem disableRipple value={value} key={value}>
          {label}
        </MenuItem>
      ))}
    </StyledSelect>
  </SelectWrapper>
);

SortBy.propTypes = {
  handler: PropTypes.func.isRequired,
  value: PropTypes.string
};

export default SortBy;

const SelectWrapper = styled.div`
  display: flex;
  flex: 1 0 40%;
  padding: 5px;
  padding-bottom: 10px;

  @media (min-width: 768px) {
    flex: 0 0 25%;
    margin-right: 1rem;
    padding: unset;
  }
`;

const StyledSelect = styled(withStyles({ icon: { fill: "#d8d8d8" } })(Select))`
  && {
    color: ${props => props.theme.inputTextColor};
    background: ${props => props.theme.inputBackgroundColor};
    height: ${props => props.theme.inputHeight};
    padding: 0.25rem;
    width: 100%;
    font-size: ${props => props.theme.inputFontSize};

    &:before {
      border-bottom: none;
    }

    &:after {
      border-bottom: none;
    }
  }
`;
