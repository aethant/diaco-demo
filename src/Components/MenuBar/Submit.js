import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Button from "@material-ui/core/Button";

/**
 * Render submission button for application filters
 *
 * @prop {Function} handler         handler to fire when button onClick triggered by user
 */
const Submit = ({ handler }) => (
  <ButtonWrapper>
    <StyledButton variant="contained" disableRipple onClick={handler}>
      Search
    </StyledButton>
  </ButtonWrapper>
);

Submit.propTypes = {
  handler: PropTypes.func.isRequired
};

export default Submit;

const ButtonWrapper = styled.div`
  display: flex;
  flex: 1 1 20%;
`;

const StyledButton = styled(Button)`
  && {
    font-size: ${props => props.theme.inputFontSize};
    background-color: ${props => props.theme.buttonBackgroundColor};
    color: ${props => props.theme.inputTextColor};
    transition: ${props => props.theme.buttonTransition};
    text-transform: none;
    width: 100%;
    margin-right: 5px;
    margin-bottom: 8px;

    @media (min-width: 768px) {
      margin: unset;
      margin-left: auto;
    }

    &:hover {
      background-color: ${props => props.theme.buttonBackgroundColor};
      opacity: ${props => props.theme.buttonHoverOpacity};
      transition: ${props => props.theme.buttonTransition};
    }
  }
`;
