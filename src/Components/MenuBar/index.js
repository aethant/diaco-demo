import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";

import Search from "Components/MenuBar/Search";
import SortBy from "Components/MenuBar/SortBy";
import Submit from "Components/MenuBar/Submit";

/**
 * Render application menu "filter" bar
 *
 * @prop {Function} handleTextUpdate              handler for search text input
 * @prop {Function} handleSortUpdate              handler for sort by select input
 * @prop {Function} handleFormSubmit              handler for submission of filter form
 * @prop {String}   searchText                    value for search text input
 * @prop {String}   sortByValue                   value for sort by select input
 */
const MenuBar = ({
  handleTextUpdate,
  handleSortUpdate,
  handleFormSubmit,
  searchText,
  sortByValue
}) => (
  <StyledAppBar position="static">
    <StyledToolbar>
      <Search handler={handleTextUpdate} value={searchText} />
      <SortBy handler={handleSortUpdate} value={sortByValue} />
      <Submit handler={handleFormSubmit} />
    </StyledToolbar>
  </StyledAppBar>
);

MenuBar.propTypes = {
  handleTextUpdate: PropTypes.func.isRequired,
  handleSortUpdate: PropTypes.func.isRequired,
  handleFormSubmit: PropTypes.func.isRequired,
  searchText: PropTypes.string,
  sortByValue: PropTypes.string
};

export default MenuBar;

const StyledAppBar = styled(AppBar)`
  && {
    background-color: ${props => props.theme.menubarBackgroundColor};
    display: flex;
    justify-content: center;
    align-content: center;
    align-items: center;
    box-shadow: none;
  }
`;

const StyledToolbar = styled(Toolbar)`
  && {
    max-width: ${props => props.theme.maxContainerWidth};
    width: inherit;
    display: flex;
    flex: 1;
    justify-content: space-between;
    flex-wrap: wrap;

    @media (min-width: 768px) {
      flex-wrap: nowrap;
    }
  }
`;
