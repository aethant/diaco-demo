import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Input from "@material-ui/core/Input";

/**
 * Render text search input
 *
 * @prop {Function} handler       handle input into component
 * @prop {String}   value         value applied to component
 */
const Search = ({ handler, value }) => (
  <InputWrapper>
    <StyledInput
      placeholder="Some search term"
      onChange={handler}
      value={value}
      type="search"
    />
  </InputWrapper>
);

Search.propTypes = {
  handler: PropTypes.func.isRequired,
  value: PropTypes.string
};

export default Search;

const InputWrapper = styled.div`
  display: flex;
  flex: 100%;
  padding: 5px;
  padding-top: 10px;

  @media (min-width: 768px) {
    flex: 1 1 50%;
    margin-right: 1rem;
    padding: unset;
  }
`;

const StyledInput = styled(Input)`
  && {
    background: ${props => props.theme.inputBackgroundColor};
    height: ${props => props.theme.inputHeight};
    color: ${props => props.theme.inputTextColor};
    font-size: ${props => props.theme.inputFontSize};
    width: 100%;
    padding: 5px;
    border-radius: 2px;
  }
`;
