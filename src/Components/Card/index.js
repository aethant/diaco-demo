import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";

import Image from "Components/Card/Image";
import Title from "Components/Card/Title";
import Summary from "Components/Card/Summary";
import Button from "Components/Card/Button";

/**
 * Renders core Article Card component, into which are rendered subelements of the card
 *
 * @prop {String} title           Article title
 * @prop {String} urlToImage      URL pointing to header image of article
 * @prop {String} description     Summary text of article
 * @prop {String} url             URL pointing to full article
 */
const ArticleCard = ({ title, urlToImage, description, url }) => (
  <StyledCard>
    <Image url={urlToImage} title={title} />
    <StyledCardContent>
      <Title label={title} />
      <Summary text={description} />
      <Button url={url} />
    </StyledCardContent>
  </StyledCard>
);

ArticleCard.propTypes = {
  title: PropTypes.string,
  urlToImage: PropTypes.string,
  description: PropTypes.string,
  url: PropTypes.string
};

export default ArticleCard;

const StyledCard = styled(Card)`
  && {
    display: flex;
    flex: auto;
    flex-direction: column;
    height: ${props => props.theme.cardHeight};
    margin: 0.5rem;
    padding-bottom: 4rem;
    border-radius: 0;
    box-shadow: none;

    @media (min-width: 768px) {
      flex: 47.5%;
    }

    @media (min-width: 960px) {
      flex: 48%;
    }
  }
`;

const StyledCardContent = styled(CardContent)`
  && {
    display: flex;
    flex-direction: column;
    flex: 100%;
    justify-content: flex-start;
    align-content: space-evenly;
    position: relative;
  }
`;
