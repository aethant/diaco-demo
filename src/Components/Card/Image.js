import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import CardMedia from "@material-ui/core/CardMedia";

/**
 * Render article card header image
 *
 * @prop {String} url           URL pointing to article header image
 * @prop {String} title         Article title to use as image descriptor
 */
const Image = ({ url, title }) => (
  <StyledCardMedia image={url} title={title} alt={title} />
);

Image.propTypes = {
  url: PropTypes.string,
  title: PropTypes.string
};

Image.defaultProps = {
  url: "",
  title: ""
};

export default Image;

const StyledCardMedia = styled(CardMedia)`
  && {
    flex: 0 0 50%;
    background-position: center center;
    background-repeat: no-repeat;
    background-color: ${props => props.theme.defaultImageBackgroundColor};
    background-size: cover;
  }
`;
