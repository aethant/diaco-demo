import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Typography from "@material-ui/core/Typography";

/**
 * Render article summary text
 *
 * @prop {String} text      Summary description of article
 */
const Summary = ({ text }) => (
  <StyledSummary gutterBottom component="h2" variant="h6">
    {text}
  </StyledSummary>
);

Summary.propTypes = {
  text: PropTypes.string
};

Summary.defaultProps = {
  text: ""
};

export default Summary;

const StyledSummary = styled(Typography)`
  && {
    line-height: 1.5;
    font-size: 0.95rem;
    font-weight: 400;
    flex-grow: 1;
    flex-shrink: 0;
    flex-basis: 50%;
    margin-bottom: 1rem;
  }
`;
