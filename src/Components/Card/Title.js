import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Typography from "@material-ui/core/Typography";

/**
 * Render article title
 * Convert string to HTML for instances of inline HTML tags in title
 *
 * @prop {String} label     Article title
 */
const Title = ({ label }) => (
  <StyledTitle
    gutterBottom
    component="h3"
    variant="h5"
    dangerouslySetInnerHTML={{ __html: label }}
  />
);

Title.propTypes = {
  label: PropTypes.string
};

Title.defaultProps = {
  label: ""
};

export default Title;

const StyledTitle = styled(Typography)`
  && {
    flex: 0 0 25%;
    min-height: 4.25rem;
    font-size: 1.2rem;
    font-weight: 700;
    margin-bottom: 1.5rem;
    margin-top: auto;
  }
`;
