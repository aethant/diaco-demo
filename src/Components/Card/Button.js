import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Button from "@material-ui/core/Button";

/**
 * Render "Read More" button for article card
 *
 * @prop {String} url         URL pointing to original article content
 */
const ReadMore = ({ url }) => (
  <StyledButton component="a" href={url} disableRipple variant="contained">
    Read more
  </StyledButton>
);

ReadMore.propTypes = {
  url: PropTypes.string
};

ReadMore.defaultProps = {
  url: ""
};

export default ReadMore;

const StyledButton = styled(Button)`
  && {
    background-color: ${props => props.theme.inputBackgroundColor};
    color: ${props => props.theme.inputTextColor};
    flex-grow: 0;
    flex-shrink: 0;
    box-shadow: none;
    text-transform: none;
    margin-bottom: auto;
    max-width: 10rem;
    transition: all ease-in 0.2s;

    &:hover {
      background-color: ${props => props.theme.inputBackgroundColor};
      opacity: ${props => props.theme.buttonHoverOpacity};
      transition: ${props => props.theme.buttonTransition};
    }
  }
`;
