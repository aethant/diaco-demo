import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Card from "Components/Card";

/**
 * Renders container into which application will render individual article "cards"
 * If no articles are available, renders a generic "no results" message out to user
 * Omits rendering articles that lack both a title and a description
 *
 * @prop {Object|Array} articles        `articles` portion of response object from API
 */
const Content = ({ articles }) => {
  const articleKeys = articles ? Object.keys(articles) : [];
  const hasArticles = articles && articleKeys.length;
  return (
    <ContentWrapper>
      <CardsWrapper>
        <CardsContent>
          {hasArticles ? (
            articleKeys.map(
              key =>
                (articles[key].title && articles[key].description && (
                  <Card key={key} {...articles[key]} />
                )) ||
                null
            )
          ) : (
            <div>No results</div>
          )}
        </CardsContent>
      </CardsWrapper>
    </ContentWrapper>
  );
};

Content.propTypes = {
  articles: PropTypes.oneOfType([PropTypes.array, PropTypes.object])
};

export default Content;

const ContentWrapper = styled.main`
  margin: 0 auto;
  display: flex;
  width: inherit;
  justify-content: center;
  min-height: 100vh;

  @media (min-width: 768px) {
    padding-left: 1.5rem;
    padding-right: 1.5rem;
  }

  @media (min-width: 960px) {
    max-width: 960px;
  }
`;

const CardsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  justify-content: center;
  margin-bottom: 2rem;

  @media (min-width: 768px) {
    justify-content: space-evenly;
  }
`;

const CardsContent = styled.div`
  display: flex;
  margin-top: 2rem;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
  align-content: center;
`;
