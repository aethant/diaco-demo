import React, { PureComponent } from "react";
import fetch from "isomorphic-fetch";
import { ThemeProvider } from "styled-components";
import styled from "styled-components";

import Constants from "Utilities/Constants";
import Theme from "Utilities/Theming";
import MenuBar from "Components/MenuBar";
import Content from "Components/Content";

/**
 * Core Application container, which controls application data flow and
 * interactions.
 *
 * Retrieves data on load-in and submission of filter operations
 * Pass article data to Content pane
 *
 * @class Application
 * @extends {PureComponent}
 */
class Application extends PureComponent {
  state = {
    language: "en", // by default, language is "en" (per req documentation)
    text: "", // by default, no search is affected
    sort: "none", // by default, no sorting is affected
    articles: {} // by default, no articles are available until a fetch completes
  };

  /**
   * Build out params to affix to the endpoint, if available, when a search takes
   * place based on user submission
   *
   * @memberOf Application
   */
  _buildSearchParams = () => {
    const { text, sort } = this.state;
    const hasSearchValues = Boolean(text || sort);
    if (!hasSearchValues) {
      return "";
    }

    return `${text ? `&q=${text}` : ""}${
      text && sort && sort !== "none" ? `&sortBy=${sort}` : "" // if no text, there's nothing to sort
    }`;
  };

  /**
   * Perform a data fetch from the appropriate endpoint to gather articles
   * Returns a JSON object, as described by API documentation on success.
   * Throws error object on failure.
   *
   * @memberOf Application
   */
  _fetchDataFromServer = () => {
    const searchQuery = this._buildSearchParams();
    const server = searchQuery
      ? Constants.newsAPIEverythingURL
      : Constants.newsAPITopHeadlinesURL;

    return fetch(`${server}&language=en${searchQuery}`)
      .then(response => response.json())
      .then(json => json)
      .catch(err => {
        throw new Error(err);
      });
  };

  /**
   * Handle submission of filter form by user
   *
   *
   * @memberOf Application
   */
  _handleSearchSubmit = () =>
    this._fetchDataFromServer().then(({ articles }) =>
      this.setState(state => ({ ...state, articles }))
    );

  /**
   * Handle inputting of text into the search term input
   *
   *
   * @memberOf Application
   */
  _handleSearchTextInput = ({ target: { value: text } }) =>
    this.setState(state => ({
      ...state,
      text,
      sort: text.length ? state.sort : "none" // if no text, nothing to sort, so reset the "sort by" value to "none"
    }));

  /**
   * Handle changing value of sort by input select
   *
   *
   * @memberOf Application
   */
  _handleSortByInput = ({ target: { value: sort } }) =>
    this.setState(state => ({
      ...state,
      sort
    }));

  componentDidMount() {
    this._fetchDataFromServer().then(({ articles }) =>
      this.setState(state => ({
        ...state,
        articles
      }))
    );
  }

  render() {
    const { text, sort, articles } = this.state;
    return (
      <ThemeProvider theme={Theme}>
        <ApplicationWrapper>
          <MenuBar
            handleTextUpdate={this._handleSearchTextInput}
            handleSortUpdate={this._handleSortByInput}
            handleFormSubmit={this._handleSearchSubmit}
            searchText={text}
            sortByValue={sort}
          />
          <Content articles={articles} />
        </ApplicationWrapper>
      </ThemeProvider>
    );
  }
}

export default Application;

const ApplicationWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
`;
