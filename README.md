## Installation

After cloning this repo, run `yarn install` to install all necessary dependencies.

## Usage

To run the demo, use the following inside the project root folder:

`$ REACT_APP_NEWSAPI_KEY=<YOURKEY> yarn start`

## Technologies Employed

- react
- styled-components
- material-ui
- isomorphic-fetch
- babel-plugin-module-resolver

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
